package com.awesomeapps.awesomeimageapp.data;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

import com.awesomeapps.awesomeimageapp.model.ImageResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thays Santos Duarte on 06/11/2017.
 */
public class DataManagerTest {

    @Mock
    RemoteDataSource mockRemoteDataSource;

    @Mock
    GetImagesCallback mockGetImagesCallback;

    @Captor
    ArgumentCaptor<GetImagesCallback> getImagesCallbackCaptor;

    private DataManager dataManager;

    @Before
    public void setup() {
        dataManager = DataManager.getInstance();
    }

    @After
    public void tearDown() {
        dataManager.destroyInstance();
    }

    @Test
    public void getImages() throws Exception {

    }

    @Test
    public void getPhotos_shouldCallRemoteDataSourceAndStoreLocally() {
        int page = 1;
        String query = "cat";

        dataManager.getImages(query, page, mockGetImagesCallback);

        verify(mockRemoteDataSource).getImages(eq(query), eq(page),
                getImagesCallbackCaptor.capture());

        List<ImageResult> images = new ArrayList<>();
        getImagesCallbackCaptor.getValue().onSuccess(images);

        verify(mockGetImagesCallback).onSuccess(images);
    }

}