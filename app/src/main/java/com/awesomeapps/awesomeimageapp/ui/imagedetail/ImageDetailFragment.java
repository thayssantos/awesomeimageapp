package com.awesomeapps.awesomeimageapp.ui.imagedetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.awesomeapps.awesomeimageapp.R;
import com.awesomeapps.awesomeimageapp.model.ImageResult;
import com.awesomeapps.awesomeimageapp.util.BaseFragmentInteractionListener;
import com.awesomeapps.awesomeimageapp.util.DownloadImage;
import com.awesomeapps.awesomeimageapp.util.common.Constants;
import com.awesomeapps.awesomeimageapp.util.mvp.BaseView;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public class ImageDetailFragment extends BaseView {
    private BaseFragmentInteractionListener fragmentInteractionListener;
    private ImageResult imageResult;
    private ImageView fullImage;
    private TextView imageDescription;
    private TextView imageWidth;
    private TextView imageHeight;
    private TextView imageColor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imageResult =
                    (ImageResult) getArguments().getSerializable(Constants.BUNDLE_KEY_IMAGE_RESULT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_detail, container, false);

        fullImage = (ImageView) view.findViewById(R.id.iv_full_image);
        imageDescription = (TextView) view.findViewById(R.id.tv_description);
        imageWidth = (TextView) view.findViewById(R.id.tv_width);
        imageHeight = (TextView) view.findViewById(R.id.tv_height);
        imageColor = (TextView) view.findViewById(R.id.tv_color);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (imageResult.getUrls().getRaw() != null) {
            DownloadImage.getInstance().downloadImage(imageResult.getUrls().getRegular(),
                    fullImage);
        }

        fillImageDetail();

    }

    @Override
    public void showToastMessage(String message) {
        super.showToastMessage(message);
    }

    private void fillImageDetail() {

        if (imageResult.getDescription() != null) {
            imageDescription.setText(
                    getResources().getString(R.string.description) + imageResult.getDescription());
        } else {
            imageDescription.setText(
                    getResources().getString(R.string.description) + getResources().getString(
                            R.string.no_description));
        }

        if (imageResult.getWidth() > 0) {
            imageWidth.setText(getResources().getString(R.string.width) + String.valueOf(
                    imageResult.getWidth()));
        } else {
            imageWidth.setText(
                    getResources().getString(R.string.description) + getResources().getString(
                            R.string.no_description));
        }

        if (imageResult.getHeight() > 0) {
            imageHeight.setText(getResources().getString(R.string.height) + String.valueOf(
                    imageResult.getHeight()));
        } else {
            imageHeight.setText(
                    getResources().getString(R.string.description) + getResources().getString(
                            R.string.no_description));
        }

        if (imageResult.getColor() != null) {
            imageColor.setText(getResources().getString(R.string.color) + imageResult.getColor());
        } else {
            imageColor.setText(
                    getResources().getString(R.string.description) + getResources().getString(
                            R.string.no_description));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentInteractionListener = (BaseFragmentInteractionListener) getActivity();
    }
}
