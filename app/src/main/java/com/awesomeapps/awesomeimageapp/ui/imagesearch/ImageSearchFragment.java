package com.awesomeapps.awesomeimageapp.ui.imagesearch;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.awesomeapps.awesomeimageapp.R;
import com.awesomeapps.awesomeimageapp.data.DataManager;
import com.awesomeapps.awesomeimageapp.model.ImageResult;
import com.awesomeapps.awesomeimageapp.ui.imagedetail.ImageDetailFragment;
import com.awesomeapps.awesomeimageapp.util.BaseFragmentInteractionListener;
import com.awesomeapps.awesomeimageapp.util.ImageCache;
import com.awesomeapps.awesomeimageapp.util.ImageFetcher;
import com.awesomeapps.awesomeimageapp.util.common.Constants;
import com.awesomeapps.awesomeimageapp.util.common.RecyclerViewScrollListener;
import com.awesomeapps.awesomeimageapp.util.mvp.BaseView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Thays Santos Duarte on 02/11/2017.
 */

public class ImageSearchFragment extends BaseView implements ImageSearchContract.View {

    private static final int NUM_COLUMNS = 3;
    private List<ImageResult> imageResults;
    private ImageSearchContract.Presenter presenter;
    private ImageResultRecyclerAdapter imageResultRecyclerAdapter;
    private EditText searchField;
    private RecyclerView imagesList;
    private BaseFragmentInteractionListener fragmentInteractionListener;
    private RecyclerViewScrollListener recyclerViewScrollListener;
    private View view;
    private String query = "";
    private int currentPage;
    private ImageFetcher imageFetcher;
    private int imageThumbSize;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageResults = new ArrayList<>();
        presenter = new ImageSearchPresenter(this, DataManager.getInstance());

        configCacheImage();
    }

    private void configCacheImage() {
        imageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(getActivity(), Constants.APPLICATION_FOLDER);
        cacheParams.setMemCacheSizePercent(0.25f);

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        imageFetcher = new ImageFetcher(getActivity(), imageThumbSize);
        imageFetcher.setLoadingImage(R.mipmap.ic_placeholder);
        imageFetcher.addImageCache(getActivity().getSupportFragmentManager(), cacheParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_image_search, container, false);

        searchField = (EditText) view.findViewById(R.id.et_search_field);
        imagesList = (RecyclerView) view.findViewById(R.id.rv_images_list);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), NUM_COLUMNS);
        imagesList.setLayoutManager(gridLayoutManager);
        imageResultRecyclerAdapter = new ImageResultRecyclerAdapter(this, imageResults,
                imageFetcher);
        imagesList.setAdapter(imageResultRecyclerAdapter);

        createDebounceEditText(searchField);
        this.currentPage = 1;

        recyclerViewScrollListener = new RecyclerViewScrollListener(gridLayoutManager,
                imageFetcher) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                getImages(query, page);

                final int curSize = imageResultRecyclerAdapter.getItemCount();

                view.post(new Runnable() {
                    @Override
                    public void run() {
                        imageResultRecyclerAdapter.notifyItemRangeInserted(curSize,
                                imageResultRecyclerAdapter.getItemCount() - 1);
                    }
                });
            }
        };

        imagesList.addOnScrollListener(recyclerViewScrollListener);

    }

    private void createDebounceEditText(EditText searchField) {
        fromView(searchField)
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String text) throws Exception {
                        if (text.isEmpty()) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                })
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String result) throws Exception {
                        if (!result.equals(query)) {
                            query = result;
                            currentPage = 1;
                        }
                        clearInfo();
                        presenter.getImages(result, currentPage);
                    }
                });
    }

    public Observable<String> fromView(EditText editText) {

        final PublishSubject<String> subject = PublishSubject.create();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 3) {
                    subject.onNext(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                subject.onComplete();
            }
        });

        return subject;
    }

    private void clearInfo() {
        imageResultRecyclerAdapter.clearList();
        recyclerViewScrollListener.resetState();
    }

    private void hideKeyboard() {
        if (getContext() != null && getContext().INPUT_METHOD_SERVICE != null) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void showImages(List<ImageResult> images) {
        imageResultRecyclerAdapter.addImages(images);
        hideKeyboard();
    }

    private void getImages(String query, int page) {
        this.currentPage++;
        presenter.getImages(query, this.currentPage);
    }

    public void showDetailFragment(int imagePosition) {
        ImageResult imageResult = imageResults.get(imagePosition);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_KEY_IMAGE_RESULT, imageResult);
        fragmentInteractionListener.showFragment(ImageDetailFragment.class, bundle, true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentInteractionListener = (BaseFragmentInteractionListener) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        imageFetcher.setExitTasksEarly(false);
        imageResultRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        imageFetcher.setPauseWork(false);
        imageFetcher.setExitTasksEarly(true);
        imageFetcher.flushCache();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imageFetcher.closeCache();
    }
}
