package com.awesomeapps.awesomeimageapp.ui.imagesearch;

import com.awesomeapps.awesomeimageapp.model.ImageResult;
import com.awesomeapps.awesomeimageapp.util.mvp.IBasePresenter;
import com.awesomeapps.awesomeimageapp.util.mvp.IBaseView;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 02/11/2017.
 */

interface ImageSearchContract {

    interface View extends IBaseView {
        void showImages(List<ImageResult> images);
    }

    interface Presenter extends IBasePresenter<View> {
        void getImages(String query, int page);
    }
}
