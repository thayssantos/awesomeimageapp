package com.awesomeapps.awesomeimageapp.ui.imagesearch;

import android.util.Log;

import com.awesomeapps.awesomeimageapp.R;
import com.awesomeapps.awesomeimageapp.data.DataManager;
import com.awesomeapps.awesomeimageapp.data.GetImagesCallback;
import com.awesomeapps.awesomeimageapp.model.ImageResult;
import com.awesomeapps.awesomeimageapp.util.mvp.BasePresenter;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 03/11/2017.
 */

public class ImageSearchPresenter extends BasePresenter<ImageSearchContract.View> implements
        ImageSearchContract.Presenter {

    private final String TAG = getClass().getSimpleName();
    private DataManager dataManager;

    public ImageSearchPresenter(ImageSearchContract.View view, DataManager dataManager) {
        this.view = view;
        this.dataManager = dataManager;
    }

    @Override
    public void getImages(String query, int page) {
        if (view != null) {
            dataManager.getImages(query, page, new GetImagesCallback() {
                @Override
                public void onSuccess(List<ImageResult> images) {
                    if (images.size() > 0) {
                        view.showImages(images);
                    } else {
                        view.showToastMessage(view.getContext().getResources().getString(
                                R.string.image_not_found));
                    }
                }

                @Override
                public void onFailure(Throwable throwable) {
                    Log.e(TAG, throwable.getMessage());
                    view.showToastMessage(view.getContext().getResources().getString(
                            R.string.image_not_found));
                }

                @Override
                public void onNetworkFailure() {

                }
            });

        }
    }
}
