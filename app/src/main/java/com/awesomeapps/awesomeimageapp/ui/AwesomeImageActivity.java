package com.awesomeapps.awesomeimageapp.ui;

import android.Manifest;
import android.os.Bundle;

import com.awesomeapps.awesomeimageapp.R;
import com.awesomeapps.awesomeimageapp.ui.imagesearch.ImageSearchFragment;
import com.awesomeapps.awesomeimageapp.util.BaseActivity;
import com.awesomeapps.awesomeimageapp.util.BaseFragmentInteractionListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

/**
 * Created by Thays Santos Duarte on 02/11/2017.
 */

public class AwesomeImageActivity extends BaseActivity implements BaseFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_awesome_image);
        showFragment(ImageSearchFragment.class);

        MultiplePermissionsListener dialogMultiplePermissionsListener =
                DialogOnAnyDeniedMultiplePermissionsListener.Builder
                        .withContext(this)
                        .withTitle(getResources().getString(R.string.permission_title))
                        .withMessage(getResources().getString(R.string.permission_message))
                        .withButtonText(android.R.string.ok)
                        .withIcon(R.mipmap.ic_launcher)
                        .build();

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(dialogMultiplePermissionsListener).check();
    }

}
