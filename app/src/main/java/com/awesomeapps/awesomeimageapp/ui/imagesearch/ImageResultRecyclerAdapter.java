package com.awesomeapps.awesomeimageapp.ui.imagesearch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.awesomeapps.awesomeimageapp.R;
import com.awesomeapps.awesomeimageapp.model.ImageResult;
import com.awesomeapps.awesomeimageapp.util.ImageFetcher;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public class ImageResultRecyclerAdapter extends
        RecyclerView.Adapter<ImageResultRecyclerAdapter.ViewHolder> {

    private final ImageFetcher imageFetcher;
    private List<ImageResult> imageResults;
    private ImageSearchFragment fragment;

    public ImageResultRecyclerAdapter(ImageSearchFragment fragment,
            List<ImageResult> imageResults, ImageFetcher imageFetcher) {
        this.fragment = fragment;
        this.imageResults = imageResults;
        this.imageFetcher = imageFetcher;
    }

    @Override
    public ImageResultRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
            int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_image_result, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        ImageResult imageResult = imageResults.get(position);

        if (viewHolder == null) {
            viewHolder.ivImage = new RecyclingImageView(fragment.getContext());
            viewHolder.ivImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

        imageFetcher.loadImage(imageResult.getUrls().getThumb(), viewHolder.ivImage);

        viewHolder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.showDetailFragment(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageResults.size();
    }

    public void addImages(List<ImageResult> images) {
        imageResults.addAll(images);
        notifyDataSetChanged();
    }

    public void clearList() {
        imageResults.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View itemView;
        private ImageView ivImage;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ivImage = (ImageView) itemView.findViewById(R.id.iv_image);
        }
    }
}
