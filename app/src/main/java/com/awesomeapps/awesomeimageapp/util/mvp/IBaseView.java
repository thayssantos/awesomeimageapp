package com.awesomeapps.awesomeimageapp.util.mvp;

import android.content.Context;

/**
 * Created by Thays Santos Duarte on 02/11/2017.
 */

public interface IBaseView {

    void showToastMessage(String message);

    void setProgressBar(boolean show);

    Context getContext();
}
