package com.awesomeapps.awesomeimageapp.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.awesomeapps.awesomeimageapp.model.ImageResult;
import com.awesomeapps.awesomeimageapp.ui.imagesearch.ImageResultRecyclerAdapter;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Thays Santos Duarte on 06/11/2017.
 */

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    private final ImageResultRecyclerAdapter.ViewHolder viewHolder;
    private final DownloadImage downloadImage;
    private WeakReference<ImageView> imageViewWeakReference;
    private ImageResult imageResult;
    private int position;

    public DownloadImageTask(ImageView imageView, ImageResult imageResult, int position,
            ImageResultRecyclerAdapter.ViewHolder viewHolder, DownloadImage downloadImage) {
        this.imageViewWeakReference = new WeakReference<ImageView>(imageView);
        this.imageResult = imageResult;
        this.position = position;
        this.viewHolder = viewHolder;
        this.downloadImage = downloadImage;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            return downloadBitmap(params[0]);
        } catch (Exception e) {
            // log error
        }
        return null;

    }

    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
        } catch (Exception e) {
            urlConnection.disconnect();
            Log.w("ImageDownloader", "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewWeakReference != null) {
            ImageView imageView = imageViewWeakReference.get();
            if (imageView != null) {
                if ((bitmap != null) && (imageView.getVisibility() == View.VISIBLE)
                        && (viewHolder.getAdapterPosition() == position)) {
                    imageView.setImageBitmap(bitmap);
                    downloadImage.addImageToCache(bitmap);
                } else {
//                    Drawable placeholder = imageView.getContext().getResources().getDrawable(R
// .drawable.ic_laucher, null);
//                    imageView.setImageDrawable(placeholder);
                }
            }
        }

//        if (this.imageView != null) {
//            ImageView imageView = this.imageView.get();
//            if (imageView != null) {
//                if ((bitmap != null) && (imageView.getVisibility() == View.VISIBLE)
//                        && (viewHolder.getAdapterPosition() == position)) {
//                    imageView.setImageBitmap(bitmap);
//                }
//            }
//        }
    }
}
