package com.awesomeapps.awesomeimageapp.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import com.awesomeapps.awesomeimageapp.util.common.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public class DownloadImage {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final String TAG = getClass().getSimpleName();
    private static DownloadImage instance = new DownloadImage();
    private String imageUrl;
    public static final String IMAGE_FORMAT = ".jpg";

    public static DownloadImage getInstance() {
        return instance;
    }

    private Observable<Bitmap> createObservable(final String urlImage) {
        return Observable.create(new ObservableOnSubscribe<Bitmap>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Bitmap> e) throws Exception {
                Bitmap bitmap = doDownloadImage(urlImage);
                e.onNext(bitmap);
                e.onComplete();
            }
        });
    }

    private DisposableObserver<Bitmap> createObserver(final ImageView ivImage) {
        return new DisposableObserver<Bitmap>() {
            @Override
            public void onNext(Bitmap bitmap) {
                Log.d(TAG, "onNext " + bitmap);
                ivImage.setImageBitmap(bitmap);

                addImageToCache(bitmap);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete");
            }
        };
    }

    public void addImageToCache(Bitmap bitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File applicationDir = new File(root + Constants.APPLICATION_FOLDER);
        applicationDir.mkdirs();

        String imageName = null;
        try {
            imageName = Constants.IMAGE_NAME + URLEncoder.encode(imageUrl, "UTF-8") + IMAGE_FORMAT;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        File file = new File(applicationDir, imageName);

        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void downloadImage(String imgUrl, ImageView ivImage) {

        String root = Environment.getExternalStorageDirectory().toString();
        String imagePath = null;
        try {
            imagePath = root + Constants.APPLICATION_FOLDER + "/" + Constants.IMAGE_NAME
                    + URLEncoder.encode(imgUrl, "UTF-8") + IMAGE_FORMAT;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        File imgFile = new File(imagePath);
        this.imageUrl = imgUrl;

        if (imgFile.exists()) {
            Bitmap imageBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            ivImage.setImageBitmap(imageBitmap);
        } else {
            compositeDisposable.add(createObservable(imageUrl)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(createObserver(ivImage)));
        }
    }

    private static Bitmap doDownloadImage(String imageUrl) {
        URL url = null;
        Bitmap bitmap = null;

        try {
            url = new URL(imageUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            InputStream in = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}
