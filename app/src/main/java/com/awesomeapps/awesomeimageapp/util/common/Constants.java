package com.awesomeapps.awesomeimageapp.util.common;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public final class Constants {
    public static final String APPLICATION_FOLDER = "/awesomeimageapp";
    public static final String IMAGE_NAME = "awesomeimage";

    public static final String GET_IMAGES_ENDPOINT = "search/photos/";

    public static final String QUERY_PARAM_CLIENT_ID = "client_id";
    public static final String QUERY_PARAM_QUERY = "query";
    public static final String QUERY_PARAM_PER_PAGE = "per_page";
    public static final String QUERY_PARAM_PAGE = "page";

    public static final String BUNDLE_KEY_IMAGE_RESULT = "bundle_key_image_result";
}
