package com.awesomeapps.awesomeimageapp.util.mvp;

/**
 * Created by Thays Santos Duarte on 02/11/2017.
 */

public interface IBasePresenter<ViewT> {

    void onViewActive(ViewT view);

    void onViewInactive();
}
