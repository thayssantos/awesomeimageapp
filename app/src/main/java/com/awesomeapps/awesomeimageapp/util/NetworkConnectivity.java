package com.awesomeapps.awesomeimageapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Thays Santos Duarte on 03/11/2017.
 */

public class NetworkConnectivity {
    private static NetworkConnectivity networkConnectivity;

    public static synchronized NetworkConnectivity getInstance() {
        if (networkConnectivity == null) {
            networkConnectivity = new NetworkConnectivity();
        }
        return networkConnectivity;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
}
