package com.awesomeapps.awesomeimageapp.data;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public interface IDataManager {

    void getImages(String query, int page, GetImagesCallback getImagesCallback);
}
