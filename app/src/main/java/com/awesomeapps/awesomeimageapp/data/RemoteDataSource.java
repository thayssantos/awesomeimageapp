package com.awesomeapps.awesomeimageapp.data;

import android.util.Log;

import com.awesomeapps.awesomeimageapp.BuildConfig;
import com.awesomeapps.awesomeimageapp.model.Response;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public class RemoteDataSource {

    private final String TAG = getClass().getSimpleName();
    private static final int PER_PAGE = 12;
    private Retrofit retrofitClient;
    private ImagesApiService imagesApiService;
    private CompositeDisposable compositeDisposable;

    public RemoteDataSource() {
        retrofitClient = createRetrofit();
        imagesApiService = retrofitClient.create(ImagesApiService.class);
        compositeDisposable = new CompositeDisposable();
    }

    public void getImages(String query, int page, GetImagesCallback getImagesCallback) {
        compositeDisposable.add(createObservable(query, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(createObserver(getImagesCallback)));
    }

    private Observable<Response> createObservable(String query, int page) {
        return imagesApiService.getImagesSearch(BuildConfig.UNSPLASH_CLIENT_ID, query, PER_PAGE,
                page);
    }

    private DisposableObserver<Response> createObserver(final GetImagesCallback getImagesCallback) {
        return new DisposableObserver<Response>() {
            @Override
            public void onNext(Response response) {
                getImagesCallback.onSuccess(response.getImageResults());
            }

            @Override
            public void onError(Throwable e) {
                getImagesCallback.onFailure(e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete");
            }
        };
    }

    private Retrofit createRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.UNSPLASH_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    private OkHttpClient createOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }
}
