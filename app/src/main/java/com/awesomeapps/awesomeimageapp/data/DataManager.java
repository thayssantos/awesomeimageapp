package com.awesomeapps.awesomeimageapp.data;

/**
 * Created by Thays Santos Duarte on 03/11/2017.
 */

public class DataManager implements IDataManager {
    private RemoteDataSource remoteDataSource;
    private static DataManager instance = new DataManager();

    public static DataManager getInstance() {
        return instance;
    }

    private DataManager() {
        remoteDataSource = new RemoteDataSource();
    }

    @Override
    public void getImages(String query, int page, GetImagesCallback getImagesCallback) {
        remoteDataSource.getImages(query, page, getImagesCallback);
    }

    public void destroyInstance() {
        remoteDataSource = null;
    }
}
