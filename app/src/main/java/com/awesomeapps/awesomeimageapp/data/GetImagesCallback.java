package com.awesomeapps.awesomeimageapp.data;

import com.awesomeapps.awesomeimageapp.model.ImageResult;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public interface GetImagesCallback {

    void onSuccess(List<ImageResult> images);

    void onFailure(Throwable throwable);

    void onNetworkFailure();
}
