package com.awesomeapps.awesomeimageapp.data;

import com.awesomeapps.awesomeimageapp.model.Response;
import com.awesomeapps.awesomeimageapp.util.common.Constants;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public interface ImagesApiService {

    @GET(Constants.GET_IMAGES_ENDPOINT)
    Observable<Response> getImagesSearch(
            @Query(Constants.QUERY_PARAM_CLIENT_ID) String clientId,
            @Query(Constants.QUERY_PARAM_QUERY) String query,
            @Query(Constants.QUERY_PARAM_PER_PAGE) int perPage,
            @Query(Constants.QUERY_PARAM_PAGE) int page);
}
